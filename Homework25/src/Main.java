import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Main {
    //language=SQL
    private static final String SQL_SELECT_FROM_FOODSTUFFS = "select * from foodstuffs order by id";
    private static final String SQL_SELECT_FROM_FOODSTUFFS_PRICE = "select food_name, price from foodstuffs where price > 100";
    private static final String SQL_SELECT_FROM_FOODSTUFFS_DELIVERY_DATE = "select food_name, delivery_date from foodstuffs where delivery_date < '2022-02-02'";
    private static final String SQL_SELECT_FROM_FOODSTUFFS_SUPPLIER = "select food_name, supplier from foodstuffs";

    public static void main(String[] args) {
        Properties properties = null;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        try (Connection connection = DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));
             Statement statement = connection.createStatement()){

            System.out.println("***ВСЯ ТАБЛИЦА***");
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_FOODSTUFFS)){
                while (resultSet.next()) {
                    String foodName = ((ResultSet) resultSet).getString("food_name");
                    int price = resultSet.getInt("price");
                    int expirationDate = resultSet.getInt("expiration_date");
                    String deliveryDate = resultSet.getString("delivery_date");
                    String supplier = resultSet.getString("supplier");

                    System.out.println(foodName + " " + price + " " + expirationDate + " " + deliveryDate + " " + supplier);
                }
            }
            System.out.println();
            System.out.println("***ТОВАРЫ ДОРОЖЕ 100 РУБ***");
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_FOODSTUFFS_PRICE)){
                while (resultSet.next()) {
                    String foodName = resultSet.getString("food_name");
                    int price = resultSet.getInt("price");

                    System.out.println(foodName + " " + price);
                }
            }

            System.out.println();
            System.out.println("***ТОВАРЫ ПОСТАВЛЕННЫЕ РАНЕЕ 2022-02-02***");
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_FOODSTUFFS_DELIVERY_DATE)){
                while (resultSet.next()) {
                    String foodName = resultSet.getString("food_name");
                    String deliveryDate = resultSet.getString("delivery_date");

                    System.out.println(foodName + " " + deliveryDate);
                }
            }

            System.out.println();
            System.out.println("***ВСЕ ПОСТАВЩИКИ***");
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_FOODSTUFFS_SUPPLIER)){
                while (resultSet.next()) {
                    String foodName = resultSet.getString("food_name");
                    String supplier = resultSet.getString("supplier");

                    System.out.println(foodName + " " + supplier);
                }
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
