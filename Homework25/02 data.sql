insert into foodstuffs (food_name, price, expiration_date, delivery_date, supplier)
values ('Молоко', 67, 14, '2022-02-14', 'Алабуга Сотэ');
insert into foodstuffs (food_name, price, expiration_date, delivery_date, supplier)
values ('Сырок', 38, 21, '2022-02-01', 'РостАгроКомплекс');
insert into foodstuffs (food_name, price, expiration_date, delivery_date, supplier)
values ('Соус Heinz', 79, 365, '2022-02-15', 'ПетроПрдукт-Отрадное');
insert into foodstuffs (food_name, price, expiration_date, delivery_date, supplier)
values ('Йогурт греческий', 57, 21, '2022-01-30', 'ТД МолКом');
insert into foodstuffs (food_name, price, expiration_date, delivery_date, supplier)
values ('Шоколад Горький', 141, 365, '2022-02-14', 'Гагаринские Мануфактуры');
insert into foodstuffs (food_name, price, expiration_date, delivery_date, supplier)
values ('Хлеб тостовый', 58, 7, '2022-02-05', 'Кондитерский Комбинат');
insert into foodstuffs (food_name, price, expiration_date, delivery_date, supplier)
values ('Сыр Гауда', 171, 120, '2022-02-01', 'Проект 21');
insert into foodstuffs (food_name, price, expiration_date, delivery_date, supplier)
values ('Ветчина', 194, 30, '2022-01-31', 'Пятачок');
insert into foodstuffs (food_name, price, expiration_date, delivery_date, supplier)
values ('Картофельные чипсы', 89, 180, '2021-12-20', 'СнэкПрот');
insert into foodstuffs (food_name, price, expiration_date, delivery_date, supplier)
values ('Макароны', 140, 730, '2021-11-10', 'Диетика');