create table foodstuffs
(
    id serial primary key ,
    food_name char (30) not null ,
    price int not null check ( price > 0 ),
    expiration_date int not null check ( expiration_date > 0 ) ,
    delivery_date date not null ,
    supplier char (30) not null
);