import java.util.Random;

public class Program {
    public static void main(String[] args) {
        Random ran = new Random();
        Human[] humans = new Human[50];
        int[] ages = new int[100];

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            humans[i].name = "User" + i;
            humans[i].age = ran.nextInt(100);

        }
        for (int i = 0; i < humans.length; i++) {
            int currentAge = humans[i].age;
            ages[currentAge]++;
        }
        for (int i = 0; i < humans.length; i++) {
            System.out.println("Людей, с возрастом " + i + " - " + ages[i] + " штук");
        }
    }

    public static class Human {
        String name;
        int age;
    }
}
