drop table foodstuffs;

-- товары дороже 100 руб
select food_name, price from foodstuffs
where price > 100;

-- товары поставленые ранее 2022-02-02
select food_name, delivery_date from foodstuffs
where delivery_date < '2022-02-02';

-- все поставщики
select food_name, supplier from foodstuffs;

