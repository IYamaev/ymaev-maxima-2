import java.util.Arrays;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int [] array = new int[10];
        for (int i = 0; i < array.length; i++){
            array[i] = sc.nextInt();
        }
        System.out.println(Arrays.toString(array));
        for (int i = array.length - 1; i >= 0; i--){
            System.out.print(array[i] + " ");
        }
    }
}
