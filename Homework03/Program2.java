import java.util.Scanner;

public class Program2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int left = 0;
        int middle = number;
        int right = 0;
        int count = 0;
        while (number != 0) {
            if (left > middle && middle < right) {
                count++;
            }
            left = middle;
            middle = right;
            right = number;
            number = sc.nextInt();
        }
        System.out.println(count);
    }
}
