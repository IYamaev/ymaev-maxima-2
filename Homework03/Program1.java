import java.util.Scanner;

public class Program1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int sum = 0;
        int readNumber = 0;
        int min = number;
        int minDigitSum = Integer.MAX_VALUE;

        while (number != 0) {
            readNumber = number;

            while (number != 0) {
                int lastDigit = number % 10;
                number = number / 10;
                sum = sum + lastDigit;

            }
            if (minDigitSum > sum){
                minDigitSum = sum;
                min = readNumber;
            }
            sum = 0;
            number = sc.nextInt();
        }
        System.out.println(min);
    }
}
