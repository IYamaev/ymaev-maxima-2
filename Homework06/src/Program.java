import java.util.Arrays;
import java.util.Scanner;

public class Program {
    public static void selectionSort(int[] array) {
        int min, indexOfMin;
        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
    }

    public static boolean binarySearch(int[] array) {
        Scanner sc = new Scanner(System.in);
        int element = sc.nextInt();
        boolean hasElement = false;
        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;
        while (left <= right) {
            if (element < array[middle]) {
                right = middle - 1;
                middle = left + (right - left) / 2;
            } else if (element > array[middle]) {
                left = middle + 1;
                middle = left + (right - left) / 2;
            } else {
                hasElement = true;
                break;
            }
        }
        return hasElement;
    }

    public static void main(String[] args) {
        // Реализовать в Program процедуру void selectionSort(int[] array) и функцию boolean search(int[] array)
        // с бинарным поиском.
        int[] myArray = {6, 42, 3, 7, 16, 10, 22};
        System.out.println(Arrays.toString(myArray));
        selectionSort(myArray);
        System.out.println(Arrays.toString(myArray));
        System.out.println(binarySearch(myArray));
    }
}
