
public class Driver {
    private String name;
    private int experience;
    private Bus bus;

    public Driver(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;
        } else {
            System.err.println("Опыта у " + name + " нет");
        }
    }

    public void drive() {

    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }
}
