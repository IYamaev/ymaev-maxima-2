
public class Passenger {
    private String name;

    // объектная переменная, поле, которое ссылается на какой-то автобус
    private Bus bus;

    public Passenger(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void goToBus(Bus bus) {
        // проверяем, а не в автобусе ли мы уже?
        if (this.bus != null) {
            System.err.println("Я, " + name + ", в автобусе");
        } else {
            if (!bus.isFull()) {
                // если автобуса ещё не было
                this.bus = bus;
                // передаём в автобус себя
                this.bus.incomePassenger(this);
            } else {
                System.err.println("Я, " + name + ", не попал в автобус");
            }
        }
    }
}

